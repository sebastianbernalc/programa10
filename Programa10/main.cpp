#include <iostream>
#include <stdlib.h>
#include <string.h>
using namespace std;

int main()
{
     int Arreglo_Numerico[100],Contador_Arreglo_Numerico=0,Suma_Romanos=0;

     char Arreglo_Entrada[100];
     cout<<"Ingrese el numero romano: \n";
     cin>>Arreglo_Entrada;


     for(int i=0;Arreglo_Entrada[i]!='\0';i++){     //La condicion es que recorra el arreglo hasta encontrar el \0 que es hasta donde hay almacenado la cadena de caracteres
         if(Arreglo_Entrada[i]=='M' ||Arreglo_Entrada[i]=='m' ){  //Compara cada posicion de la cadena de caracteres pertenece a tal numero romano
             Arreglo_Numerico[Contador_Arreglo_Numerico]=1000;      //Si es asi le da el valor asignado al numero romano
             Contador_Arreglo_Numerico++;       //Y si lo encuentra le suma +1 al contador i que ira recorriendo la cadena de caracteres
         }
         if(Arreglo_Entrada[i]=='D' || Arreglo_Entrada[i]=='d'){
             Arreglo_Numerico[Contador_Arreglo_Numerico]=500;
             Contador_Arreglo_Numerico++;
         }
         if(Arreglo_Entrada[i]=='C'|| Arreglo_Entrada[i]=='c'){
             Arreglo_Numerico[Contador_Arreglo_Numerico]=100;
             Contador_Arreglo_Numerico++;
         }
         if(Arreglo_Entrada[i]=='L'|| Arreglo_Entrada[i]=='l'){
             Arreglo_Numerico[Contador_Arreglo_Numerico]=50;
             Contador_Arreglo_Numerico++;
         }
         if(Arreglo_Entrada[i]=='X'|| Arreglo_Entrada[i]=='x'){
             Arreglo_Numerico[Contador_Arreglo_Numerico]=10;
             Contador_Arreglo_Numerico++;
         }
         if(Arreglo_Entrada[i]=='V'|| Arreglo_Entrada[i]=='v'){
             Arreglo_Numerico[Contador_Arreglo_Numerico]=5;
             Contador_Arreglo_Numerico++;
         }
         if(Arreglo_Entrada[i]=='I'|| Arreglo_Entrada[i]=='i'){
             Arreglo_Numerico[Contador_Arreglo_Numerico]=1;
             Contador_Arreglo_Numerico++;
         }
     }

    Suma_Romanos=Arreglo_Numerico[0];       //toma la posicion del numero mayor para hacer la operacion
    for(int i=0;i<Contador_Arreglo_Numerico-1;i++){     //recorre todas las posiciones para mirar si suma o resta
        if(Arreglo_Numerico[i+1]<=Arreglo_Numerico[i]){ //condicion si el numero que sigue es mayor

            Suma_Romanos=Suma_Romanos+Arreglo_Numerico[i+1];
        }
        else{
            if(Suma_Romanos<Arreglo_Numerico[i+1]){ //Condicion para saber que numero debe ir `positivo en caso de la resta para que por algun caso no de un (--) que en ese caso sumarian
            Suma_Romanos=-Suma_Romanos+Arreglo_Numerico[i+1];
            }
            else {
                Suma_Romanos=Suma_Romanos-Arreglo_Numerico[i+1];
            }
        }
    }
     cout<<"El numero ingresado fue: "<<strupr(Arreglo_Entrada)<<endl<<"Que corresponde a: "<<Suma_Romanos<<endl;
     return 0;
}
